package cn.jiangjiesheng.testdemo;

public class DemoService {
    private String name;

    private int age;

    // Getter and Setter ToString


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
