package cn.jiangjiesheng.provider.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Value("${server.port}")
    private String port;

    //IDEA一个服务启动多个实例的方法
    //https://blog.csdn.net/u012069313/article/details/122925739
    @GetMapping("/sayHi")
    public String sayHi() {
        return "Hi,我来自端口：" + port;
    }
}
