package cn.jiangjiesheng.consumer.feignClient.client;

import cn.jiangjiesheng.consumer.feignClient.config.FeignMultipartSupportConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "${biz-service-provider.server.name:service-provider}"
        , fallback = HelloFeignClientHystrix.class
        , configuration = FeignMultipartSupportConfig.class)
public interface HelloFeignClient {

    @RequestMapping(value = "/sayHi", method = RequestMethod.GET)
    String sayHi();

}