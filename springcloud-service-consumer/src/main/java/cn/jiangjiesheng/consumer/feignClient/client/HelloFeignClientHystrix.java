package cn.jiangjiesheng.consumer.feignClient.client;
import org.springframework.stereotype.Component;

@Component
public class HelloFeignClientHystrix implements HelloFeignClient {

    @Override
    public String sayHi() {
        return "调用失败";
    }
}
