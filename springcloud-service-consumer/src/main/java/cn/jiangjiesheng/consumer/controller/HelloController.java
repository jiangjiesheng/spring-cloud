package cn.jiangjiesheng.consumer.controller;

import cn.jiangjiesheng.consumer.feignClient.client.HelloFeignClient;
import cn.jiangjiesheng.consumer.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @Autowired //RestTemplate + ribbon
    HelloService helloService;

    @Autowired //feign
    private HelloFeignClient helloFeignClient;


    @RequestMapping(value = "/sayHi")
    public String sayHi() {
        return helloService.sayHi();
    }
    @RequestMapping(value = "/sayHiByFeign")
    public String sayHiByFeign() {
        return helloFeignClient.sayHi();
    }
}
